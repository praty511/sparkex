package oi

import org.apache.spark.rdd.RDD
import org.apache.spark.sql.SparkSession

object HelloRDD {

  def main(args: Array[String]) {
    val spark = SparkSession
      .builder()
      .appName("Hello OpenInsights")
      .master("local[*]")
      .getOrCreate()

    val numbersRDD: RDD[Int] = spark.sparkContext.parallelize(List(1, 2, 3, 4, 5))
    val squaresRDD: RDD[Int] = numbersRDD.map(x => x * x)
    val squares: Array[Int] = squaresRDD.collect()
    squares.foreach(println)

    spark.stop()
  }
  def count(lines:RDD[String]): RDD[(String,Int)] = {
    lines.flatMap(line => line.split(" "))
      .map(word => (word, 1))
      .reduceByKey(_ + _)
  }
}
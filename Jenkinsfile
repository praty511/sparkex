pipeline {
    agent any

    parameters {
     gitParameter branchFilter: 'origin/(.*)', defaultValue: 'master', name: 'BRANCH', type: 'PT_BRANCH', description: 'Enter the required branch'
    }

    triggers{
        GenericTrigger(causeString: 'Generic Cause', genericVariables: [
                [defaultValue: '', key: 'BRANCH', regexpFilter: '', value: '$.push.changes[0].new.name']
                ], 
                printContributedVariables: true, 
                printPostContent: true, 
                regexpFilterExpression: '(master|dev|release)$', 
                regexpFilterText: '$BRANCH', 
                token: 'mytokendataspark', 
                tokenCredentialId: '')
    }

    environment {
        NEXUS_VERSION = "nexus3"
        NEXUS_PROTOCOL = "https"
        NEXUS_URL = "ci.open-insights.co.in:8444"
        NEXUS_REPOSITORY = "hermes-snapshots"
        NEXUS_CREDENTIAL_ID = "nexus-creds"
        mailToRecipients = 'pratyush@open-insights.com'
        userAborted = false
    }

    tools {
        // Install the Maven version configured as "M3" and add it to the path.
        maven "maven-3.5.2"
    }

    options {
        // This is required if you want to clean before build
        skipDefaultCheckout(true)
        // timeout(time: 3, unit: 'MINUTES')
    }


    stages {

        // stage('get approval'){
        //         when { expression { return env.BRANCH == 'master'} }
        //         steps{
        //             echo "sending mail"
        //             approve()
        //                 script{
        //                     try {
        //                         userInput = input submitter: 'vagrant', message: 'Do you approve?'
        //                     }catch (org.jenkinsci.plugins.workflow.steps.FlowInterruptedException e) {
        //                         cause = e.causes.get(0)
        //                         echo "Aborted by " + cause.getUser().toString()
        //                         userAborted = true
        //                         echo "SYSTEM aborted, but looks like timeout period didn't complete. Aborting."
        //                     }
        //                     if (env.userAborted.toBoolean() == true) {
        //                         currentBuild.result = 'ABORTED'
        //                         echo "SYSTEM aborted, but looks like timeout period didn't complete. Aborting."
        //                     } else {
        //                         echo "Building"
        //                     }
        //                 } 
        //         }
        //     } 

        stage('Git Pull') {
            steps {
                // Get some code from a GitHub repository
                cleanWs()
                git branch: "${params.BRANCH}", credentialsId: 'pratyushJenkins', url: 'https://bitbucket.org/praty511/sparkex.git'

            }
            
        }

        stage('Build'){
            steps{
                    //build command
                    sh "echo building"
                    sh "mvn -Dmaven.test.failure.ignore=true clean package"
            }
        }

        stage('Sonar Report'){
            steps{
                //generating sonar report 
                sh "echo 'generating sonar report'"
                withSonarQubeEnv('sonarqube') {
   //                        withMaven(maven: 'maven-3.5.2'){
                               sh "mvn install sonar:sonar"
    //                        }
                        }
            }
        }

        stage('Nexus Upload'){
            steps{
                //pushing to nexus repository
                echo 'pushing to nexus repository'

                nexusArtifactUploader(
                            nexusVersion: NEXUS_VERSION,
                            protocol: NEXUS_PROTOCOL,
                            nexusUrl: NEXUS_URL,
                            groupId: 'com.openinsights',
                            version: '2.0.2-SNAPSHOT',
                            repository: NEXUS_REPOSITORY,
                            credentialsId: NEXUS_CREDENTIAL_ID,
                            artifacts: [
                                [artifactId: 'lbs-dataspark',
                                classifier: '',
                                file: 'target/lbs-dataspark-2.0.2-SNAPSHOT.jar',
                                 type: 'jar'],
                                [artifactId: 'lbs-dataspark',
                                classifier: '',
                                file: "pom.xml",
                                type: "pom"]
                            ]
                        );
            }
        }
    }

    post {
    
          success { 
            notifySuccessfull()
        echo "Success"
            }
            failure{
            sh " cat /data/jenkins/jobs/singtel-devops/jobs/sgt-test/jobs/lbs-dataspark/builds/${BUILD_NUMBER}/log >> logtext.txt "
            sh " awk '/FAILED*/ || /ERROR*/ || /Warning*/ || /failed/ || /msg/' logtext.txt > logs.txt "
           
                notifyFailed()
         echo "failed"
            }
        }
}

  def approve(){
        emailext (
        body: '''
        This email is sent by jenkins requesting your approval for building '${env.JOB_NAME.substring(JOB_NAME.lastIndexOf('/') + 1, JOB_NAME.length())} [${env.BUILD_NUMBER}]'.
        Please go to console output of ${BUILD_URL}input to approve or Reject.<br>
        ''',
        mimeType: 'text/html',
       // subject: "APPROVE: Job '${env.JOB_NAME} [${env.BUILD_NUMBER}]'",
        subject: "Pipeline '${env.JOB_NAME.substring(JOB_NAME.lastIndexOf('/') + 1, JOB_NAME.length())} [${env.BUILD_NUMBER}]' Build Approval Request",
        to: "${env.DEFAULT_RECIPIENTS}, pratyush02j@gmail.com" ,
      //  to: "pratyush02j@gmail.com",
        recipientProviders: [[$class: 'CulpritsRecipientProvider']]
        )
    }
    
    def notifyStarted() {

        emailext (
        subject: "STARTED: Job '${env.JOB_NAME} [${env.BUILD_NUMBER}]'",
        body: """<p>Job '${env.JOB_NAME} [${env.BUILD_NUMBER}]' has started to build</p>
        <p>Check console output at <a href='${env.BUILD_URL}'>${env.JOB_NAME} [${env.BUILD_NUMBER}]</a></p>""",
        recipientProviders: [[$class: 'DevelopersRecipientProvider']],
        to: "${env.DEFAULT_RECIPIENTS}"
                )
        }
    
    def notifySuccessfull(){
        emailext (
        attachLog: true,
        attachmentsPattern: 'logs.txt',
        subject: "SUCCESSFUL: Job '${env.JOB_NAME.substring(JOB_NAME.lastIndexOf('/') + 1, JOB_NAME.length())} [${env.BUILD_NUMBER}]'",
        body: """<p>Job '${env.JOB_NAME.substring(JOB_NAME.lastIndexOf('/') + 1, JOB_NAME.length())} [${env.BUILD_NUMBER}]' has finished with status <b> <span style="color: green">SUCCESS</span></b>.</p>
        <p>Check console output at <a href='${env.BUILD_URL}'>${env.JOB_NAME} [${env.BUILD_NUMBER}]</a></p>""",
        recipientProviders: [[$class: 'DevelopersRecipientProvider']],
      //  to: "${env.DEFAULT_RECIPIENTS}, amit@open-insights.com"
       to: "pratyush02j@gmail.com"
         )
    }
    
    def notifyFailed(){
        emailext (
        attachLog: true, 
        attachmentsPattern: 'logs.txt',
        subject: "FAILED: Job '${env.JOB_NAME.substring(JOB_NAME.lastIndexOf('/') + 1, JOB_NAME.length())} [${env.BUILD_NUMBER}]'",
        body: """<p>Job '${env.JOB_NAME.substring(JOB_NAME.lastIndexOf('/') + 1, JOB_NAME.length())} [${env.BUILD_NUMBER}]' has finished with status <b><span style="color: red">FAILURE</span></b>.</p>
        <p>Check console output <a href='${env.BUILD_URL}'>${env.JOB_NAME} [${env.BUILD_NUMBER}]</a></p>""",
      //  recipientProviders: [[$class: 'DevelopersRecipientProvider']],
       //  to: "${env.DEFAULT_RECIPIENTS}, amit@open-insights.com"
         to: "pratyush02j@gmail.com"
        )
    }